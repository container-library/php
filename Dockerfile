FROM codeberg.org/container-library/base AS target

# Install dependencies
RUN echo "@testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> "/etc/apk/repositories" &&\
    apk add --no-cache php8 $(printf "php8-%s " \
      fpm \
      bcmath calendar ctype dba exif fileinfo ftp iconv gd intl mbstring opcache pcntl pdo pdo_dblib pdo_mysql pdo_odbc pdo_pgsql pdo_sqlite phar posix session sockets sqlite3 \
      bz2 curl dom gettext gmp imap ldap xml mysqli mysqlnd odbc openssl pgsql pspell simplexml snmp soap sodium tidy xml xmlreader xmlwriter xsl zip \
    ) apache2 $(printf "apache2-%s " \
      proxy utils \
    ) dma@testing tzdata &&\
    ln -s /usr/bin/php8 /usr/bin/php &&\
    wget -O /usr/local/bin/composer https://getcomposer.org/download/latest-stable/composer.phar && chmod +x /usr/local/bin/composer &&\
    wget -O /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-9.phar && chmod +x /usr/local/bin/phpunit &&\
    deluser xfs && adduser -h /var/www/html -u 33 -DHSG www-data www-data

# Apply configuration
RUN rm -rf /var/www/localhost
COPY etc/ /etc/
COPY apply-php-environment /usr/local/bin/apply-php-environment

# Setup environment
EXPOSE 80
VOLUME /var/www/html
WORKDIR /var/www/html

# Run tests in temporary stage
FROM target
COPY tests /tests
RUN phpunit /tests
FROM target

LABEL maintainer="Codeberg Container Library: https://codeberg.org/container-library"
LABEL description="Dockerized PHP container that aims to be similarly easy to use as most hosting offers, with Apache + PHP-FPM for best compatibility."
LABEL version="8.0.0"
HEALTHCHECK --interval=10s CMD ["curl", "http://127.0.0.1/.well-known/codeberg-container-library/healthcheck"]
