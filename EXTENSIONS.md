# PHP Extensions

## Core & Bundled
 Bundled | Extension           | Alpine Package
:-------:| ------------------- | --------------
 Yes ✓   | BC Math             | `php8-bcmath`
 Yes ✓   | Calendar            | `php8-calendar`
 \-      | COM                 | - (Windows only)
 Yes ✓   | Ctype               | `php8-ctype`
 Yes ✓   | DBA                 | `php8-dba`
 Yes ✓   | Exif                | `php8-exif`
 Yes ✓   | Fileinfo            | `php8-fileinfo`
 Yes ✓   | FTP                 | `php8-ftp`
 Yes ✓   | iconv               | `php8-iconv`
 Yes ✓   | GD                  | `php8-gd`
 Yes ✓   | intl                | `php8-intl`
 \-      | JSON                | -
 Yes ✓   | Multibyte String    | `php8-mbstring`
 Yes ✓   | OPCache             | `php8-opcache`
 Yes ✓   | PCNTL               | `php8-pcntl`
 \-      | PCRE                | -
 Yes ✓   | PDO                 | `php8-pdo`
 Yes ✓   | POSIX               | `php8-posix`
 Yes ✓   | Sessions            | `php8-session`
 \-      | Semaphore           | -
 \-      | Shared Memory       | -
 Yes ✓   | Sockets             | `php8-sockets`
 Yes ✓   | SQLite3             | `php8-sqlite3`
 \-      | XML-RPC             | -
 \-      | Zlib                | -

## External

 Bundled | Extension             | Alpine Package
:-------:| --------------------- | --------------
 Yes ✓   | Bzip2
 Yes ✓   | cURL
 \-      | dBase
 Yes ✓   | DOM
 \-      | Enchant
 \-      | FrontBase
 Yes ✓   | Gettext
 Yes ✓   | GMP
 \-      | Firebird/InterBase
 Yes ✓   | IMAP
 Yes ✓   | LDAP
 Yes ✓   | libxml
 \-      | Mcrypt
 \-      | Mhash
 \-      | MySQL (Original)
 Yes ✓   | MySQLi
 Yes ✓   | Mysqlnd
 \-      | OCI8
 Yes ✓   | ODBC
 Yes ✓   | OpenSSL
 Yes ✓   | PDO: MS SQL Server    | `php8-pdo_dblib`
 \-      | PDO: Firebird         | ?
 Yes ✓   | PDO: MySQL            | `php8-pdo_mysql`
 \-      | PDO: Oracle           | ?
 Yes ✓   | PDO: ODBC and DB2     | `php8-pdo_odbc`
 Yes ✓   | PDO: PostgreSQL       | `php8-pdo_pgsql`
 Yes ✓   | PDO: SQLite           | `php8-pdo_sqlite`
 Yes ✓   | PostgreSQL
 Yes ✓   | Pspell
 \-      | Readline
 \-      | Recode
 Yes ✓   | SimpleXML
 Yes ✓   | SNMP
 Yes ✓   | SOAP
 Yes ✓   | Sodium
 Yes ✓   | Tidy
 \-      | WDDX
 \-      | XML Parser
 Yes ✓   | XMLReader
 Yes ✓   | XMLWriter
 Yes ✓   | XSL
 Yes ✓   | Zip

## PECL

 Bundled | Extension           | Alpine Package
:-------:| ------------------- | --------------
 \-      | Classkit
 \-      | CUBRID
 \-      | DB++
 \-      | Direct IO
 \-      | Eio
 \-      | Ev
 \-      | Event
 \-      | Expect
 \-      | FDF
 \-      | filePro
 \-      | Gearman
 \-      | Gender
 \-      | GeoIP
 \-      | Gmagick
 \-      | GnuPG
 \-      | IBM DB2
 \-      | ImageMagick
 \-      | Ingres
 \-      | Inotify
 \-      | Lua
 \-      | LuaSandbox
 \-      | LZF
 \-      | Mailparse
 \-      | Memcache
 \-      | Memcached
 \-      | Memtrack
 \-      | Mongo
 \-      | mqseries
 \-      | mysqlnd_memcache
 \-      | mysqlnd_ms
 \-      | mysqlnd_mux
 \-      | mysqlnd_qc
 \-      | mysqlnd_uh
 \-      | Ncurses
 \-      | OAuth
 \-      | OpenAL
 \-      | Paradox
 \-      | PDO: CUBRID
 \-      | PDO: IBM
 \-      | PDO: Informix
 \-      | Proctitle
 \-      | PS
 \-      | pthreads
 \-      | Quickhash
 \-      | Radius
 \-      | Rar
 \-      | RRD
 \-      | runkit7
 \-      | ScoutAPM
 \-      | Seaslog
 \-      | Solr
 \-      | Sphinx
 \-      | SQLSRV
 \-      | ssdeep
 \-      | SSH2
 \-      | Statistics
 \-      | Stomp
 \-      | Swoole
 \-      | SVM
 \-      | SVN
 \-      | Taint
 \-      | TCP
 \-      | tokyo_tyrant
 \-      | Trader
 \-      | V8js
 \-      | Varnish
 \-      | win32service
 \-      | WinCache
 \-      | xattr
 \-      | xdiff
 \-      | Xhprof
 \-      | Yaf
 \-      | Yaml
 \-      | YAZ
 \-      | ZooKeeper

## Additional Tools

- Composer
- PHPUnit 9
