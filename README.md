# PHP Container Image
> PHP is a popular general-purpose scripting language that is especially suited to web development. (https://www.php.net/)

Container image for PHP that aims to be similarly easy to use as most hosting offers, with [Apache](https://httpd.apache.org/) + [PHP-FPM](https://www.php.net/manual/de/intro.fpm.php) for best compatibility & speed.

## Use it for your application
```bash
docker run -d -p 80:80 -v "/home/example/my-homepage:/var/www/html" codeberg.org/container-library/php
```

## Volumes
### `/var/www/html`
Document root of the site to serve.

## PHP Extensions
Most basic PHP extensions are already installed, more can be added as Alpine packages using `apk add --no-cache ...`; you can find a full list of extensions in [EXTENSIONS.md](EXTENSIONS.md).

## Modifying `php.ini`
You can set environment variables starting with `php_` to set raw `php.ini` options (you can find a full list of supported options in the [list of php.ini directives](https://www.php.net/manual/ini.list.php)). For options containing a dot, replace it with `__`:

```
php_upload_max_filesize=15M
php_cli__prompt="-> "
```

## Email Configuration
Sending Email is handled using the [DragonFly Mail Agent](https://github.com/corecode/dma). You can set the "From" email address using the `php_sendmail_from` environment variable. To add an SMTP smarthost for delivery, use the following options:

- `php_smtp` - SMTP server hostname
- `php_smtp_port` - SMTP server port (default: 587, or 465 when using SSL/TLS)
- `php_smtp_secure` - if non-empty, use SSL/TLS
- `php_smtp_starttls` - if non-empty, use StartTLS (overrides `php_smtp_secure`)
- `php_smtp_username` & `php_smtp_password` - smarthost credentials

## Timezone Configuration
Setting `php_date__timezone` will change the time zone of PHP as well as the container's system timezone.
