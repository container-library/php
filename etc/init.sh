#!/bin/sh

# Apply php.ini variables from environment
/usr/local/bin/apply-php-environment

# Ensure everything in /var/www belongs to the correct user
chown -R www-data:www-data /var/www
